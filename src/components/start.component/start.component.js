import {
  formFields,
  create_multiple_question,
} from "../forms.module/forms.module.js";
import * as apiTrivia from "../../services/api.trivia.js";

export const state = {
  questions: [],
  count: 0,
  points: 0,
  current_question: {},
  amount: 0,
};

function start(e) {
  let intro = document.getElementById("intro");
  intro.style.display = "none";

  let questions = apiTrivia.get_questions(
    formFields.amount,
    formFields.category,
    formFields.difficulty,
    formFields.type
  );
  questions
    .then((r) => r.json())
    .then((r) => r.results)
    .then((r) => {
      state.questions = r;
      state.count += 1;
      state.amount = r.length;
      create_multiple_question(state.questions[0]);
    })
    .catch((e) => console.error(e));
}

export function __init__() {
  let button = document.getElementById("button-start");
  button.addEventListener("click", start);
}
