import * as sl from "../selector.component/selector.component.js";
import { range } from "../../utils/utils.js";

var config = null;

$.getJSON("../../../../metadata.json", function (data) {
  config = data;
});

export const formFields = {
  amount: "1",
  category: "9",
  difficulty: "easy",
  type: "multiple",
};
const events = [
  function select_amount(event) {
    formFields.amount = event.target.value;
  },

  function select_category(event) {
    formFields.category = event.target.value;
  },

  function select_difficulty(event) {
    formFields.difficulty = event.target.value;
  },

  function select_type(event) {
    formFields.type = event.target.value;
  },
];

/**
 * @param {string} id The target element in view
 */
export function __init__(id) {
  let target = document.getElementById(id);
  range(0, Object.keys(config).length).map((index) => {
    let cl = Object.keys(config)[index];
    let row = document.createElement("row");
    let col = document.createElement("col-12");
    let label = document.createElement("label");
    let label_text = document.createElement("h3");
    label_text.innerHTML = cl;
    label.appendChild(label_text);
    col.appendChild(label);
    row.appendChild(col);
    target.appendChild(row);
    target.appendChild(
      sl.__init__(
        config[cl].values,
        config[cl].names,
        events[index],
        "MainSelect"
      )
    );
  });
}
