import { state } from "../../start.component/start.component.js";
import { range } from "../../utils/utils.js";

function next() {
  let mquestion = document.getElementById("Mquestion");
  let bcontent = mquestion.getElementsByClassName("row")[0];
  bcontent.remove();
  setTimeout(() => {
    if (state.count < state.amount) {
      create_question(state.questions[state.count]);
      state.count += 1;
    } else {
      let intro = document.getElementById("intro");
      intro.style.display = "block";
      alert(
        "You is in the end!!! \n  Your score is: " +
          (100 * state.points) / state.amount +
          "%"
      );
      state.amount = 0;
      state.count = 0;
      state.points = 0;
    }
  }, 100);
}

function report() {
  let mquestion = document.getElementById("Mquestion");
  let content = mquestion.getElementsByClassName("col-6");
  range(0, content.length).map((index) => {
    let btn = content[index].getElementsByClassName("btn")[0];

    if (btn.c == "y") {
      btn.style["background-color"] = "green";
      btn.removeEventListener("click", response);
    } else {
      btn.style["background-color"] = "red";
      btn.removeEventListener("click", response);
    }
  });

  let _continue = document.createElement("div");
  _continue.className = "col-12";
  let text_continue = document.createElement("h2");
  text_continue.innerHTML = "Continue";
  _continue.appendChild(text_continue);
  let row = mquestion.getElementsByClassName("row")[0];
  row.appendChild(_continue);
  _continue.id = "continue";
  _continue.addEventListener("click", next);
}

function response(e) {
  if (e.target.nodeName == "DIV") {
    e.target.style.opacity = "0.5";
  } else {
    e.target.parentNode.style.opacity = "0.5";
  }

  if (e.target.c == "y") {
    report();
    state.points += 1;
    setTimeout((e) => {
      alert("Congratulations!!! \n Your answer is correct");
    }, 100);
  } else {
    report();
    setTimeout((e) => {
      alert("Oops!!! \n Your answer is correct");
    }, 100);
  }
}

export function create_question(data_question) {
  state.current_question = data_question;
  let question = document.createElement("div");
  question.className = "col-12";
  question.id = "question";
  let question_text = document.createElement("h3");
  question_text.innerText = data_question.question;
  question.appendChild(question_text);

  let mquestion = document.getElementById("Mquestion");

  if (data_question.type == "multiple") {
    var schedule = [
      { c: "y", value: data_question.correct_answer },
      { c: "n", value: data_question.incorrect_answers[0] },
      { c: "n", value: data_question.incorrect_answers[1] },
      { c: "n", value: data_question.incorrect_answers[2] },
    ].sort(() => 0.5 - Math.random());
  } else {
    if (data_question.correct_answer == "True") {
      var schedule = [
        { c: "y", value: data_question.correct_answer },
        { c: "n", value: data_question.incorrect_answers[0] },
      ];
    } else {
      var schedule = [
        { c: "n", value: data_question.incorrect_answers[0] },
        { c: "y", value: data_question.correct_answer },
      ];
    }
  }

  let row = document.createElement("div");
  let col11 = document.createElement("div");
  let col12 = document.createElement("div");
  let col21 = document.createElement("div");
  let col22 = document.createElement("div");
  let btn11 = document.createElement("div");
  let btn12 = document.createElement("div");
  let btn21 = document.createElement("div");
  let btn22 = document.createElement("div");
  let h11 = document.createElement("h3");
  let h12 = document.createElement("h3");
  let h21 = document.createElement("h3");
  let h22 = document.createElement("h3");
  row.className = "row";
  col11.className = "col-6";
  col12.className = "col-6";
  col21.className = "col-6";
  col22.className = "col-6";
  col11["c"] = schedule[0].c;
  col12["c"] = schedule[1].c;

  if (data_question.type == "multiple") {
    col21["c"] = schedule[2].c;
    col22["c"] = schedule[3].c;
  }

  btn11.className = "btn";
  btn12.className = "btn";
  btn21.className = "btn";
  btn22.className = "btn";
  h11.innerText = schedule[0].value;
  h12.innerText = schedule[1].value;
  if (data_question.type == "multiple") {
    h21.innerText = schedule[2].value;
    h22.innerText = schedule[3].value;
  }

  h11["c"] = schedule[0].c;
  h12["c"] = schedule[1].c;
  if (data_question.type == "multiple") {
    h21["c"] = schedule[2].c;
    h22["c"] = schedule[3].c;
  }
  btn11["c"] = schedule[0].c;
  btn12["c"] = schedule[1].c;
  if (data_question.type == "multiple") {
    btn21["c"] = schedule[2].c;
    btn22["c"] = schedule[3].c;
  }
  btn11.addEventListener("click", response);
  btn12.addEventListener("click", response);
  if (data_question.type == "multiple") {
    btn21.addEventListener("click", response);
    btn22.addEventListener("click", response);
  }

  btn11.appendChild(h11);
  btn12.appendChild(h12);

  if (data_question.type == "multiple") {
    btn21.appendChild(h21);
    btn22.appendChild(h22);
  }

  col11.appendChild(btn11);
  col12.appendChild(btn12);
  if (data_question.type == "multiple") {
    col21.appendChild(btn21);
    col22.appendChild(btn22);
  }

  row.appendChild(question);
  row.appendChild(col11);
  row.appendChild(col12);
  if (data_question.type == "multiple") {
    row.appendChild(col21);
    row.appendChild(col22);
  }

  mquestion.appendChild(row);
}
