import { range } from "../../utils/utils.js";

/**
 * @param {?Array<string>} values The values for event select
 * @param {?Array<string>} names The names for innerHtml
 * @param {Function} function The function for event change select
 * @param {string} custom_class The class
 */
export function __init__(
  values = null,
  names = null,
  callback = null,
  custom_class = Select
) {
  if (values == null || names == null) {
    console.error(
      "Is imposible create select beacause values or names is null"
    );
    return null;
  } else {
    if (values.length != names.length) {
      console.error(
        "Is imposible create select because values length must be equal to names length"
      );
      return null;
    } else {
      let select = document.createElement("select");
      select.className = custom_class;
      range(0, values.length).map((i) => {
        let option = document.createElement("option");

        option.value = values[i];
        option.innerHTML = names[i];
        select.appendChild(option);
      });
      if (callback) {
        select.addEventListener("change", callback);
      }
      return select;
    }
  }
}
