import * as mf from "./main.form.component/main.form.component.js";
import * as mq from "./multiple.question.component/multiple.question.component.js";
export const formFields = mf.formFields;

export let create_multiple_question = mq.create_question;

export function __init__() {
  mf.__init__("mainForm");
}
