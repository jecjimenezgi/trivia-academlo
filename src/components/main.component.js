import * as Form from "./forms.module/forms.module.js";
import * as Start from "./start.component/start.component.js";

function __init__() {
  $(function () {
    $(".Form").load(
      "./src/components/forms.module/main.form.component/main.form.component.html"
    );
    $(".Start").load("./src/components/start.component/start.component.html");
    $(".MultipleQuestion").load(
      "./src/components/forms.module/multiple.question.component/multiple.question.component.html"
    );
  });
  setTimeout(function () {
    Form.__init__();
    Start.__init__();
  }, 500);
}

export let init = __init__();
