var myHeaders = new Headers();
myHeaders.append("Content-Type", "text/plain; charset=UTF-8");

var requestOptions = {
  method: "GET",
  headers: myHeaders,
  redirect: "follow",
};

/**
 * @param {string} number The number of the questions
 * @param {string} category The category of the questions
 * @param {string} dificultty The difficulty of the questions
 * @param {string} type The type of the questions
 */
export function get_questions(number, category, dificultty, type) {
  const endpoint = "https://opentdb.com/api.php?";
  let query =
    endpoint +
    "amount=" +
    number +
    "&category=" +
    category +
    "&difficulty=" +
    dificultty +
    "&type=" +
    type;

  return fetch(query, requestOptions);
}
